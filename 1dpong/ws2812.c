/*********************************************************************
WS2812 library for PIC24F

System clock should be configured to run at 32MHz,
all assembly instructions were designed to run at this speed

Author: Toon Peters <toon.peters@tass.be>
*********************************************************************/

#include "ws2812.h"

void ws2812_send(uint8_t *buffer, uint16_t buf_size)
{
    uint8_t saved_ipl;
    SET_AND_SAVE_CPU_IPL(saved_ipl, 0b111); //Disable user interrupts while running


  asm(
        "writeBufferRoutine: \n"

        "MOV.b  [%[buf]++], W0 \n"  // Put byte of buffer in W0
        // bit 7 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #7 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit7clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit7clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #7 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit7set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit7set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 6 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #6 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit6clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit6clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #6 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit6set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit6set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 5 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #5 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit5clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit5clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #5 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit5set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit5set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 4 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #4 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit4clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit4clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #4 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit4set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit4set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 3 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #3 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit3clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit3clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #3 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit3set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit3set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 2 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #2 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit2clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit2clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #2 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit2set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit2set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 1 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #1 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit1clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit1clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #1 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit1set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit1set: \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"

// bit 0 code
        "BSET   LATF, #4 \n"        // Turn bit on
        "BTSS   W0, #0 \n"          // Test how long to stay on - Skip if clear
        "GOTO   bit0clear \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit0clear: \n"
        "NOP \n"
        "BCLR   LATF, #4 \n"        // Turn bit off
        "BTSC   W0, #0 \n"          // Test how long to stay off - Skip if clear
        "GOTO   bit0set \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "NOP \n"
        "bit0set: \n"
        "NOP \n"

        "DEC    %[num], %[num] \n"
        "BRA    Z, exitWriteRoutine \n"

        "GOTO writeBufferRoutine \n"

        "exitWriteRoutine: \n"
  :
  : [buf] "r" (buffer), [num] "r" (buf_size)
  :
  );
  RESTORE_CPU_IPL(saved_ipl);
}

void ws2812_set_col(uint8_t *buffer, uint16_t buf_size, uint16_t led_id, uint8_t color)
{
    uint16_t byte_id = led_id * 3 + (color - 1);
    uint16_t i;

    for(i = 0; i < buf_size; i++)
    {
        if(i == byte_id)
            buffer[i] = 0xFF;
        else
            buffer[i] = 0x00;
    }
}